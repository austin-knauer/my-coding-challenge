Hello there!

I decided to complete the Calendar Matching assignment for all 10 points. I used JavaScript to complete this. There are two ways you can view my solution:

1. I decided to create a quick react app, considering I applied to the Front End Developer (React) position, and because I thought it would be easier to input code into it this way. You can find the app at https://austin-knauer.github.io/scheduler

2. If you'd prefer to run the code directly, you can input values directly and see their output on this fiddle: https://jsfiddle.net/austinknauer/3ckp9abw/5/

Note that I decided to go a bit further than the requirements and make the program work for a variable, limitless number of schedules rather than just two, although the instructions in the jsfiddle are only for coordinating two schedules. I have also added the JavaScript file with my solution to this repository.

Please feel free to reach out if you have any questions or if you have trouble running my solution.
