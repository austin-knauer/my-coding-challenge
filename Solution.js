// a function to combine multiple arrays

const combiner = (...args) => {
    let array = [];
    args.forEach(elem => array = array.concat(elem));
    return array;
}

// a functioon to convert time string into an integer representing minutes, from 0 to 1439

const toMinutes = (str) => {
    const parts = str.split(":");
    return (parseInt(parts[0]) * 60) + (parseInt(parts[1]));
}

// a function to convert integer representing minutes into a time string, from "00:00" to "23:59"

const toTime = (int) => {
    const afterColon = int % 60;
    const minutes = afterColon % 10;
    const tens = Math.floor(afterColon / 10);
    const hours = (int - afterColon) / 60;
    return `${hours}:${tens}${minutes}`;
}

// a function to make sure all of the bounds overlap and if they do, sort them, then use the middle values to return the most restrictive bounds between all schedules

const findBounds = (arr) => {
    for (let i = 0; i < arr.length; i += 2) {
        if ((arr[i + 2] > arr[i]) && (arr[i + 3] > arr[i + 1])) {
            return null;
        } else if ((arr[i + 2] < arr[i]) && (arr[i + 3] < arr[i + 1])) {
            return null;
        } else {
            const sorted = arr.sort((a, b) => a - b);
            const middle = sorted.length / 2;
            return [sorted[middle - 1], sorted[middle]];
        }
    }
}

// and finally a function to put it all together

const findMeetings = (duration, bounds, schedule) => {
    // this function can handle a variable number of different schedules/bounds by using a combining function to merge all of the different arrays into one as part of the function's arguments
    
    // convert the bounds to minute notation, then find the most restrictive bounds. If schedules don't overlap return message informing so.
    const trueBounds = findBounds(bounds.map(elem => toMinutes(elem)));
    if (trueBounds === null) {
        return "No possible meetings: schedules do not overlap.";
    }

    // convert each scheduled meeting to minute notation, correct for any meetings that begin or end outside the true bounds, and sort the array of meetings by start time
    const sortedSchedule = schedule.map(elem => elem.map(elem => toMinutes(elem)).map((elem, index) => 
        {if(index === 0 && elem < trueBounds[0]) {
            return trueBounds[0]
        } else if (index === 1 && elem > trueBounds[1]){
            return trueBounds[1]
        } else {
            return elem
        }})).sort((a, b) => a[0] - b[0]);

    // the final array of possible meeting times
    const possibleMeetings = [];

    /* this loop uses a start time (initially the starting bound), checks if the next scheduled meeting start time is 
    at least the meeting duration away, and if it is, adds that time span to the possible meetings array, 
    and if it is not, sets the start time to the ending of that meeting and continues the process with the next 
    scheduled meeting */
    let freeStart = trueBounds[0];
    while (freeStart < trueBounds[1]) {
        const meeting = sortedSchedule[0];
        const nextMeetingStart = sortedSchedule.length === 0 || meeting[0] > trueBounds[1] ? trueBounds[1] : meeting[0];
        if (nextMeetingStart - freeStart >= duration) {
            possibleMeetings.push([freeStart, nextMeetingStart]);
        }
        freeStart = sortedSchedule.length === 0 ? trueBounds[1] : meeting[1] < trueBounds[0] ? trueBounds[0] : meeting[1];
        sortedSchedule.shift();
    }

    if (possibleMeetings.length === 0) {
        return "No possible meetings: no common availability."
    } else {
        return possibleMeetings.map(elem => elem.map(elem => toTime(elem)));
    }
}

/* Enter values below. Values must be in this syntax:
    Calendars must be a array of scheduled meetings. 
    Each scheduled meeting is its own array of a start and end time, ie [["4:00", "6:00"], ["12:00", "14:00"]
    Times are entered as strings and in military time
    Daily bounds are a single array of a start and end time
    Meeting duration is given in minutes as an integer, ie 30, 60, 90, etc. */

const calendar1 = [];
const dailyBounds1 = ["", ""];
const calendar2 = [];
const dailyBounds2 = ["", ""];
const meetingDuration = 20;

const possibleMeetings = findMeetings(meetingDuration, combiner(dailyBounds1, dailyBounds2), combiner(calendar1, calendar2));
